package arkady.server;

import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class MessageController {
    @MessageMapping("/message/{clientId}")
    @SendTo("/client/{clientId}")
    public ServerMessage send(@DestinationVariable("clientId") String clientId, ClientMessage clientMessage) {
        System.out.println(clientMessage.getFrom() + " >> " + clientMessage.getText());
        return new ServerMessage(clientMessage.getFrom(), "Answered from server!");
    }
}
